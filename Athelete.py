from os import name
from tkinter.font import names


class Athelete:

    def __init__(self, name, stamina):
        self.name = name
        self.stamina = stamina
        self.remain_stamina = stamina

    def introduce(self):
        print("My name is %s"%self.name)

    def run(self):
        self.consume_stamina("Run", 10)

    def consume_stamina(self, action, consume_stamina):
        if self.remain_stamina >= consume_stamina :
            print("%s."%action)
            self.remain_stamina -= consume_stamina
        else :
            print(f"Can't {action.lower()}.")

    def rest(self):
        print("Rest.")
        self.remain_stamina = self.stamina

class SoccerPlayer(Athelete):
    population = 0

    def __init__(self, name, stamina):
        super().__init__(name, stamina)
        SoccerPlayer.population += 1

    def kick(self):
        self.consume_stamina("Kick", 20)

class Swimmer(Athelete):
    population = 0

    def __init__(self, name, stamina):
        super().__init__(name, stamina)
        Swimmer.population +=1

class Shooter(Athelete):
    population =0

    def __init__(self, name, stamina):
        super().__init__(name, stamina)
        Shooter.population += 1

    def run(self):
        print("I hate running")
    

def main():
    player = SoccerPlayer("db",15)
    player.introduce()
    player.run()
    player.rest()
    player.run()

if __name__ == "__main__" :
    main()
