# 스크립트 이름: count_alphabet.py
# Input:
# 임의의 문자열
# Top N
# Output:
# 개수 순서대로 Top N개의 알파벳 글자와 개수를 출력
# 제약:
# 대소문자를 구별하지 않음
# 알파벳 이외의 문자는 무시
# 개수가 동일한 알파벳은 알파벳 순으로 정렬

import argparse
from collections import Counter
import re

def get_args():
    parse = argparse.ArgumentParser()
    parse.add_argument("string")
    parse.add_argument("top_num")

    return parse.parse_args()

def get_alphabet(string):
    string = re.sub("[^a-zA-Z]","",string)
    string = string.lower()
    return string


def get_alphabet_num(string, top_num):
    alpha_list = list(string)
    alpha_list.sort()
    alpha_list_ = Counter(alpha_list).most_common(int(top_num))
    
    return alpha_list_



def main():
    args = get_args()
    string = get_alphabet(args.string)
    string = get_alphabet_num(string, args.top_num)

#출력
    for al,num in string:
        print(al," ",num)



 


if __name__ == "__main__":
    main()