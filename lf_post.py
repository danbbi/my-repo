import requests
import pandas as pd
import json


#post방식

url = "https://mapi.lfmall.co.kr/api/search/v2/categories"
datas = json.dumps({
    "aggs": [
                "saleType",
                "colors",
                "season",
                "styleYear",
                "brandGroup",
                "gender",
                "searchCategory",
                "price",
                "prop1",
                "prop2",
                "size1",
                "size2",
                "benefit",
                "review"
            ],
    "order": "popular",
    "page": 1,
    "pid": [80104],
    "size": 40,
    "tid": [80047]
})

headers = {
    "Content-Type": "application/json; charset=UTF-8"
}

res = requests.post(url, data=datas, headers=headers)

products = res.json()["results"]["products"]

result=[]

for product in products:
    brandname = product["brandName"]
    productname = product["name"]
    price = product["salePrice"]
    result.append([brandname,productname,price])


df = pd.DataFrame(data=result, columns=["brand_name","product_name","price"])
df.to_excel("lfmall_220410.xlsx")