# 스크립트 이름: print_items.py
# Input
# 하나 이상의 아이템
# --sorted 플래그 인자 (옵션)
# Output
# 아이템이 한 개만 입력된 경우: 아이템 그대로 출력
# 아이템이 두 개 입력된 경우: 아이템을 and로 연결
# 아이템이 세 개 이상 입력된 경우: 아이템을 콤마(,)로 연결하되 마지막 아이템은 and로 연결
# --sorted 플래그가 주어진 경우: 아이템을 알파벳 순으로 정렬하여 출력
# 제약
# 동일한 아이템이 두 개 이상 입력된 경우 중복을 제거하여 한 번만 출력

import argparse
from collections import Counter

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("item",nargs="+")
    parser.add_argument("--sorted",action="store_true")
 #   args = parser.parse_args()
 #   return (args.item)
    return parser.parse_args()


def duplicate_items(arg_items):
    dupli_items = []
    for item in arg_items:
        if item not in dupli_items:
          dupli_items.append(item)

    return dupli_items

def sorted_items(arg_items, sort_flag):
    if sort_flag == True:
        arg_items = sorted(arg_items)

    return arg_items


def main():
    args = get_args()
    items = duplicate_items(args.item)
    items = sorted_items(items, args.sorted)

    #결과출력
    item_count = len(items)
    
    if item_count == 1:
        print(items)
    elif item_count >= 2:
        cnt = 0
        for item in items:
            if cnt < item_count-1 :
                if cnt == 0 :
                    str = item
                else:
                    str = str + "," + item
           
            else :
                str = str + " and " + item
            cnt += 1
    print(str)





if __name__=="__main__":
    main()